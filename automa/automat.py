class Automat:
    def __init__(
            self,
            alphabet,
            states,
            initial_state,
            final_state,
            transition_function,
            reserved_words
    ):
        self.alphabet = alphabet
        self.states = states
        self.initial_state = initial_state
        self.final_state = final_state
        self.transition_function = transition_function
        self.reserved_words = reserved_words

    def join_automat(self,  second_automate):
        alphabet = list(set(self.alphabet + second_automate.alphabet))
        state = list(set(self.state + second_automate.state))
        initial_state = (self.initial_state, second_automate.initial_state)
        state_final = list(set(self.state_finaux + second_automate.state_finaux))
        transition_function = {}
        for (first_state, symbol), next_state in self.fonction_transition.items():
            transition_function[(first_state, symbol)] = (next_state, second_automate.initial_state)

        for (second_state, symbol), next_state in second_automate.fonction_transition.items():
            transition_function[(second_state, symbol)] = (self.initial_state, next_state)

        return Automat(alphabet, state, initial_state, state_final, transition_function)

    
    def is_afd(self):
        is_afd = all(len(set(
            next_state for (current_state, caracter), next_state in self.transition_function.items() if
            current_state == etat)) == 1 for etat in self.states)
        return is_afd

    def is_afn(self):
        is_afn = not self.is_afd and any(len(set(
            current_state for (current_state, caracter), next_state in self.transition_function.items() if
            next_state == state)) > 1 for state in self.states)
        return is_afn

    def is_e_afn(self):
        is_e_afn = not self.is_afd and not self.is_afn and any(
            caracter == '' for (current_state, caracter), next_state in self.transition_function.items())
        return is_e_afn
    def find_type(self):

        if self.is_afd:
            return "AFD"
        elif self.is_afn:
            return "AFN"
        elif self.is_e_afn:
            return "ε-AFN"
        else:
            return "Type of automat undetermined"
