from automa.automat import Automat


class AutomateEntiersOperateurs(Automat):
    def __init__(self, alphabet, states, initial_state, final_state, transition_function, reserved_words):
        super().__init__(alphabet, states, initial_state, final_state, transition_function, reserved_words)
    def print_output_result(self, mot):
        formatage = ''
        if mot.isdigit():
            formatage += f"<{mot}:int>"
        elif mot.isalpha() and mot not in self.reserved_words:
            formatage += f"<{mot}:var>"
        elif mot in self.alphabet:
            formatage += f"<{mot}:operator>"
        else:
            formatage += f"<{mot}:unknown>"
        return formatage
    
    def rec(self, text, separateur):
        mots = text.split(separateur)
        res = []
        for mot in mots:
            res.append(self.print_output_result(mot))
        return res


    # def recognize_words(self, text, separateur):
    #     mots = text.split(separateur)
    #     resultat: list = []
    #     for mot in mots:
    #         current_state = self.initial_state
    #         is_know = True
    #         word = [mot[i] for i in range(len(mot))]
    #         for symbol in word :
    #             if (current_state, symbol) in self.transition_function:
    #                 current_state = self.transition_function[(current_state, symbol)]
    #             else:
    #                 is_know = False
    #         if is_know and current_state in self.final_state:
    #             resultat.append(self.print_output_result(mot))
    #         else:
    #             resultat.append(f"<{mot}:unknown>")
    #     return resultat
    
    

def find():
    alphabet = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '=', '<', '>'}
    states = {'q0', 'q2', 'q3'}
    initial_state = 'q0'
    final_state = {'q3', 'q2'}
    reserved_words = {'if', 'then', 'else', 'for', 'do', 'while'}
    transition_function = {
        ('q0', '0'): 'q3',
        ('q0', '1'): 'q3',
        ('q0', '2'): 'q3',
        ('q0', '3'): 'q3',
        ('q0', '4'): 'q3',
        ('q0', '5'): 'q3',
        ('q0', '6'): 'q3',
        ('q0', '7'): 'q3',
        ('q0', '8'): 'q3',
        ('q0', '9'): 'q3',
        ('q0', '='): 'q2',
        ('q0', '>'): 'q2',
        ('q0', '<'): 'q2',
        ('q0', '/'): 'q2',
        ('q3', '0'): 'q3',
        ('q3', '1'): 'q3',
        ('q3', '2'): 'q3',
        ('q3', '3'): 'q3',
        ('q3', '4'): 'q3',
        ('q3', '5'): 'q3',
        ('q3', '6'): 'q3',
        ('q3', '7'): 'q3',
        ('q3', '8'): 'q3',
        ('q3', '9'): 'q3',
        ('q3', '='): 'q2',
        ('q3', '>'): 'q2',
        ('q3', '<'): 'q2',
        ('q3', '/'): 'q2',
    }
    automate_operateur = AutomateEntiersOperateurs(alphabet, states, initial_state, final_state, transition_function, reserved_words)

    # texte = "if a = 5 then 124 else 324"
    texte = input("Nb: separateur = espace (' ')\n >> Veuillez entrer un texte: ")
    mots_reconnus = automate_operateur.rec(texte, " ")
    # mots_reconnus = automate_operateur.recognize_words(texte, " ")
    print("\n Resultat:")
    print(mots_reconnus)