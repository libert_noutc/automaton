from afd import creat_automaton
from automa.find import find

while True:
    choix = input(
        "\n ================ AUTOMATON ================ \n"
        "1. Construction d’un AFD \n"
        "2. Reconnaissance des mots \n"
        "3. Union de deux AFD \n"
        "4. Reconnaissance des commentaires \n"
        ">> taper 'q' pour quitter \n"
        "============================================ \n"
        ">> Votre choix: "
    )
    if choix == "q":
        break
    elif choix == "1":
        creat_automaton()
    elif choix == "2":
        find()
    elif choix == "3":
        pass
    elif choix == "4":
        pass
    
