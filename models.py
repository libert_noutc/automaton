
class Transition:
    def __init__(self, start_state, symbol, end_state):
        self.start_state = start_state
        self.symbol = symbol
        self.end_state = end_state


class Automaton:
    def __init__(self):
        self.alphabet = []
        self.states = []
        self.initial_states = []
        self.final_states = []
        self.transitions = []
        self.epsilon = "ep"

    def join_automat(self,  second_automate):

        self.alphabet = list(set(self.alphabet + second_automate.alphabet))

        self.states = list(set(self.states + second_automate.states))

        self.initial_state = (self.initial_states,
                              second_automate.initial_states)

        self.state_final = list(
            set(self.final_states + second_automate.final_states))

        self.transitions = second_automate.transitions + self.transitions

    def set_alphabet(self, alphabet):
        self.alphabet = alphabet

    def set_states(self, states):
        self.states = states

    def add_initial_state(self, initial_states):
        self.initial_states = initial_states

    def add_final_state(self, final_states):
        self.final_states = final_states

    def add_transition(self, transition):  # start_state, symbol, end_state
        self.transitions.append(transition)

    def print_automaton(self):
        transitions = []
        for t in self.transitions:
            transitions.append(
                "("+t.start_state+", "+t.symbol+", "+t.end_state+")")
        print(
            "\n ============ AUTOMATA CRÉE ========== \n"
            "> ALPHABET E : { " + ", ".join(self.alphabet) + " }\n"
            "> ETATS Q : { " + ", ".join(self.states) + " }\n"
            "> ETATS INITIAUX qI : { " +
            ", ".join(self.initial_states) + " }\n"
            "> ETATS FINAUX qF : { " + ", ".join(self.final_states) + " }\n"
            "> TRANSITIONS \n T : { " + ", ".join(transitions) + " }\n"
        )
